<?php

namespace Drupal\entity_type_permissions\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure entity_type_permissions settings for this site.
 */
class SettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The permission handler service.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $userPermissions;

  /**
   * Constructs a SettingsForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, PermissionHandlerInterface $user_permissions, TypedConfigManagerInterface $typed_config_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->userPermissions = $user_permissions;
    parent::__construct($config_factory, $typed_config_manager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('user.permissions'),
      $container->get('config.typed') ?? NULL
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_type_permissions_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_type_permissions.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = [];
    foreach ($this->getApplicableContentEntityTypeDefinitions() as $entity_type_definition) {
      $options[$entity_type_definition->id()] = $entity_type_definition->getLabel();
    }

    $default = $this->config('entity_type_permissions.settings')->get('permissions_filter') ?? [];
    $form['permissions_filter'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Generates permissions for these entity types'),
      '#description' => $this->t('Unchecking any checkbox will clear permissions and disable access checking for this entity type.'),
      '#options' => $options,
      '#default_value' => $default,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Get a list of applicable content entity type definitions.
   *
   * @return \Drupal\Core\Entity\ContentEntityTypeInterface[]
   *   A list of applicable content entity type definitions.
   */
  protected function getApplicableContentEntityTypeDefinitions(): array {
    return array_filter($this->entityTypeManager->getDefinitions(), function (EntityTypeInterface $entity_type_definition) {
      return $entity_type_definition instanceof ContentEntityTypeInterface &&
        !$entity_type_definition->isInternal() && $entity_type_definition->getBundleEntityType();
    });
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $filter = array_filter($form_state->getValue('permissions_filter'));

    $user_role_storage = $this->entityTypeManager->getStorage('user_role');
    foreach ($this->userPermissions->getPermissions() as $permission => $info) {
      if ($info['provider'] === 'entity_type_permissions') {
        /** @var \Drupal\user\RoleInterface[] */
        $roles ??= $user_role_storage->loadMultiple();

        $result = FALSE;
        foreach ($filter as $type) {
          if (strpos($permission, $type) !== FALSE) {
            $result = TRUE;
          }
        }
        if (!$result) {
          foreach ($roles as $role) {
            $role->revokePermission($permission);
          }
        }
      }
    }

    foreach ($roles ?? [] as $role) {
      $role->save();
    }

    $this->config('entity_type_permissions.settings')
      ->set('permissions_filter', $filter)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
