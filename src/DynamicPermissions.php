<?php

namespace Drupal\entity_type_permissions;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generates dynamic bundle-specific permissions for each entity type.
 */
class DynamicPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger channel factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a DynamicPermissions object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->loggerFactory = $logger_factory;
    $this->configFactory = $config_factory;
  }

  /**
   * Check if permissions should be generated for an entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type_definition
   *   The entity type definition to check.
   *
   * @return bool
   *   TRUE if this module applies, FALSE otherwise.
   */
  public function applies(EntityTypeInterface $entity_type_definition) {
    return $entity_type_definition instanceof ContentEntityTypeInterface && !$entity_type_definition->isInternal() && $entity_type_definition->getBundleEntityType();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('config.factory'),
    );
  }

  /**
   * Get the dynamic bundle-specific permissions for each entity type.
   *
   * @return array
   *   The dynamic bundle-specific permissions for each entity type.
   */
  public function get(): array {
    $permissions = [];

    $filter = $this->configFactory->get('entity_type_permissions.settings')->get('permissions_filter') ?? [];

    foreach ($this->getApplicableContentEntityTypeDefinitions() as $entity_type_definition) {
      if (!in_array($entity_type_definition->id(), $filter)) {
        continue;
      }
      foreach ($this->getBundlesForEntityTypeDefinition($entity_type_definition) as $bundle) {
        $permissions["entity_type_permissions access {$entity_type_definition->id()} {$bundle->id()}"] = [
          'title' => $this->t('Access %entity_type_label "@bundle_label"', [
            '%entity_type_label' => $entity_type_definition->getPluralLabel(),
            '@bundle_label' => $bundle->label(),
          ]),
          'dependencies' => [
            $bundle->getConfigDependencyKey() => [
              $bundle->getConfigDependencyName(),
            ],
          ],
        ];
      }
    }

    return $permissions;
  }

  /**
   * Get a list of applicable content entity type definitions.
   *
   * @return \Drupal\Core\Entity\ContentEntityTypeInterface[]
   *   A list of applicable content entity type definitions.
   */
  protected function getApplicableContentEntityTypeDefinitions(): array {
    return array_filter($this->entityTypeManager->getDefinitions(), function (EntityTypeInterface $entity_type_definition) {
      return $entity_type_definition instanceof ContentEntityTypeInterface &&
        !$entity_type_definition->isInternal() && $entity_type_definition->getBundleEntityType();
    });
  }

  /**
   * Get a list of bundle entities for the supplied entity type definition.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type_definition
   *   The entity type for which to get a list of bundle entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   A list of bundle entities for the supplied entity type definition.
   */
  protected function getBundlesForEntityTypeDefinition(EntityTypeInterface $entity_type_definition): array {
    try {
      if ($bundle_entity_type_id = $entity_type_definition->getBundleEntityType()) {
        return $this->entityTypeManager->getStorage($bundle_entity_type_id)->loadMultiple();
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $ex) {
      // This point should never be reached, but log a message just in case.
      $this->logger()->warning('Unable to load the bundle entity type storage class for @entity_type_id; skipping dynamic permission generation', [
        '@entity_type_id' => $entity_type_definition->id(),
      ]);
    }

    return [];
  }

  /**
   * Get the logger channel for this module.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   The logger channel for this module.
   */
  protected function logger(): LoggerChannelInterface {
    return $this->loggerFactory->get('entity_type_permissions');
  }

}
