# Entity Type Permissions

Hello to all! This simple module forked from Entity Bundle Permissions but much
more useful. It allows to set up access permission for any content-based entity
type.

Differences from original module:

* You can (and should, after installation) configure permission scopes for
  different types of entities using settings. Now the permissions page looks
  simpler.
* You can easily clear unnecessary permissions via module settings. You don't
  need to use the "reverse permissions" model to remove all the unnecessary
  permissions, as in the original module.
* Permissions sorted by base entity type (Content, Comment, Media) first, and
  then by entity type (article, etc).
* Permissions model differs from original module. For example, users will only
  need to "View published content" to be able to view content, but also have
  "Access content items "Basic page"" to use operations.
* Unlike the original module, it works on both PHP7 (tested with PHP 7.4) and
  PHP 8.

(C) 2022 Andrew Answer
https://it.answe.ru | mail@answe.ru | https://t.me/andrew_answer
